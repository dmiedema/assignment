//
//  NetworkCalls.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "NetworkCalls.h"

NSString * const kAPIBaseURL = @"http://107.170.231.93/";

@implementation NetworkCalls
+ (instancetype)manager {
    static NetworkCalls *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[NetworkCalls alloc] initWithBaseURL:[NSURL URLWithString:kAPIBaseURL]];
    });
    return shared;
}

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer serializer];
        self.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

- (NSURLSessionDataTask *)GET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    void (^newFailure)(NSURLSessionDataTask *, NSError *) = ^void(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"GET error - %@, %@", error, error.userInfo);
        DDLogError(@"Task - %@", task);
        failure(task, error);
    };
    return [super GET:URLString parameters:parameters success:success failure:newFailure];
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    void (^newFailure)(NSURLSessionDataTask *, NSError *) = ^void(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"POST error - %@, %@", error, error.userInfo);
        DDLogError(@"Task - %@", task);
        failure(task, error);
    };
    return [super POST:URLString parameters:parameters success:success failure:newFailure];
}

- (NSURLSessionDataTask *)PUT:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    void (^newFailure)(NSURLSessionDataTask *, NSError *) = ^void(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"PUT error - %@, %@", error, error.userInfo);
        DDLogError(@"Task - %@", task);
        failure(task, error);
    };
    return [super PUT:URLString parameters:parameters success:success failure:newFailure];
}

- (NSURLSessionDataTask *)DELETE:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    void (^newFailure)(NSURLSessionDataTask *, NSError *) = ^void(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"DELETE error - %@, %@", error, error.userInfo);
        DDLogError(@"Task - %@", task);
        failure(task, error);
    };
    return [super DELETE:URLString parameters:parameters success:success failure:newFailure];
}

- (NSURLSessionDataTask *)PATCH:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(NSURLSessionDataTask *, id))success failure:(void (^)(NSURLSessionDataTask *, NSError *))failure {
    void (^newFailure)(NSURLSessionDataTask *, NSError *) = ^void(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"PATCH error - %@, %@", error, error.userInfo);
        DDLogError(@"Task - %@", task);
        failure(task, error);
    };
    return [super PATCH:URLString parameters:parameters success:success failure:newFailure];
}

@end
