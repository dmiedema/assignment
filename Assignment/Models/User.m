//
//  User.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic birthday;
@dynamic bodyfat;
@dynamic profilePicUrl;
@dynamic city;
@dynamic state;
@dynamic country;
@dynamic id;
@dynamic height;
@dynamic notes;
@dynamic realName;
@dynamic userId;
@dynamic weight;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic userName;

@end
