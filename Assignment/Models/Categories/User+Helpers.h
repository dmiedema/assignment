//
//  User+Helpers.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "User.h"

/// Key used in @c NSDictionary to specify feet tall. @see @c -heightDictionary
extern NSString * const kUserHeightFootKey;
/// Key used in @c NSDictionary to specify inches tall. @see @c -heightDictionary
extern NSString * const kUserHeightInchesKey;


@interface User (Helpers)
#pragma mark - Class Methods
/*!
 Create or Update a User with a given JSON dictionary in context
 
 @param data    JSON response as an @c NSDictionary
 @param context @c NSManagedObjectContext to create/update user in
 @return created/updated @c User object
 */
+ (instancetype)createOrUpdateWithData:(NSDictionary *)data inContext:(NSManagedObjectContext *)context;

#pragma mark - Instance Methods
/*!
 Get the current User's age.
 
 @return current age in years
 */
- (NSNumber *)currentAge;

/*!
 @abstract Get the users current height.
 
 @discussion since only Swift has tuples (which i would prefer for this)
 this returns a dictionary with specified key/values.
 While this could be a @c readonly property, I opted to a method for no particular reason
 
 @code
 @{kUserHeightFootKey: @6,
   kUserHeightInchesKey: @2
 };
 @endcode
 
 @return @c NSDictionary containing height in imperial format with specified key/values
 */
- (NSDictionary *)heightDictionaryImperial;
@end
