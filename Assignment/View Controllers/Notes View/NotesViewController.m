//
//  NotesViewController.m
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "NotesViewController.h"
#import "User.h"

@interface NotesViewController ()
@property (weak, nonatomic) IBOutlet UIView *backdropView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UITextView *notesTextView;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerCenterXConstraint;
@end

@implementation NotesViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dismissButton.clipsToBounds = YES;
    self.dismissButton.layer.cornerRadius = 8.0f;
    self.dismissButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.dismissButton.layer.borderWidth = 1.0f;
    
    self.containerView.clipsToBounds = YES;
    self.containerView.layer.cornerRadius = 8.0f;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.notesTextView.text = self.user.notes;
}

- (void)viewDidAppear:(BOOL)animated {
    // Seems that xibs don't have a frame until viewDidAppear
    // So I'll set the frame there so the layout constraints are correct
    [super viewDidAppear:animated];
    self.view.frame = self.parentViewController.view.bounds;
    [self.view layoutIfNeeded];
}

#pragma mark - Implementation
- (void)showNotesView:(BOOL)animated {
    CGFloat duration = animated ? 0.5 : 0.0;
    self.notesTextView.text = self.user.notes;
    
    self.view.alpha = 0.0;
    self.backdropView.alpha = 0.0;
    CGAffineTransform rotate = CGAffineTransformMakeRotation(M_PI_4);
    CGAffineTransform translation =CGAffineTransformMakeTranslation(-400, 0);
    self.containerView.transform = CGAffineTransformConcat(translation, rotate);
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:duration delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.backdropView.alpha = 0.4;
        self.view.alpha = 1.0;
        self.containerView.transform = CGAffineTransformIdentity;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismissNotesView:(BOOL)animated {
    CGFloat duration = animated ? 0.5 : 0.0;
    [self.view layoutIfNeeded];
    
    [UIView animateWithDuration:duration delay:0.0 usingSpringWithDamping:0.6 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.view.alpha = 0.0;
        self.backdropView.alpha = 0.0;
        CGAffineTransform rotate = CGAffineTransformMakeRotation(M_PI_4);;
        self.containerView.transform = rotate;
        self.containerView.center = CGPointMake(CGRectGetMidX(self.view.frame), CGRectGetHeight(self.view.frame));
    
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromParentViewController];
        [self.view removeFromSuperview];
    }];
}
#pragma mark Actions
- (IBAction)dismissButtonPressed:(UIButton *)sender {
    [self dismissNotesView:YES];
}

@end
