//
//  UsersTableFooterView.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "UsersTableFooterView.h"

@interface UsersTableFooterView ()
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@end

@implementation UsersTableFooterView
#pragma mark - Class Methods
+ (instancetype)footerView {
    return [[self alloc] initWithFrame:CGRectMake(0, 0, 100, 40)];
}
#pragma mark - Instance Methods
- (void)showSpinner {
    self.label.hidden = YES;
    [self.activityIndicator startAnimating];
}

-(void)showMore {
    self.label.hidden = NO;
    self.label.text = NSLocalizedString(@"Scroll Down For More Results", @"Scroll Down For More Results");
}

- (void)showNoMore {
    self.label.hidden = NO;
    self.label.text = NSLocalizedString(@"No More Results", @"No more results");
    [self.activityIndicator stopAnimating];
}

#pragma mark Private
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.label];
        [self addSubview:self.activityIndicator];
        
        [self addConstraints:[self constraintsForLabel]];
        [self addConstraints:[self constraintsForActivityIndicator]];
    }
    return self;
}

#pragma mark - Getters
- (UILabel *)label {
    if (!_label) {
        _label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = NSLocalizedString(@"No more results", @"No more results");
        _label.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _label;
}
- (UIActivityIndicatorView *)activityIndicator {
    if (!_activityIndicator) {
        _activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        _activityIndicator.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _activityIndicator;
}

- (NSArray *)constraintsForLabel {
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeLeadingMargin multiplier:1 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeTrailingMargin multiplier:1 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.label attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.label.superview attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
    return @[leading, trailing, top, bottom];
}

- (NSArray *)constraintsForActivityIndicator {
    NSLayoutConstraint *centerX = [NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.activityIndicator.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    NSLayoutConstraint *centerY = [NSLayoutConstraint constraintWithItem:self.activityIndicator attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.activityIndicator.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
    
    return @[centerX, centerY];
}

@end
