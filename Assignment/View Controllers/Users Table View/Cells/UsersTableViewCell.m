//
//  UsersTableViewCell.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "UsersTableViewCell.h"
#import "User+Helpers.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface UsersTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@property (weak, nonatomic) IBOutlet UILabel *stateCountryLabel;

@property (weak, nonatomic) IBOutlet UIButton *noteButton;

@end


@implementation UsersTableViewCell

- (void)awakeFromNib {
    self.noteButton.clipsToBounds = YES;
    self.noteButton.layer.cornerRadius = 8.0f;
    self.noteButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.noteButton.layer.borderWidth = 1.0f;
}

- (void)setupCell:(User *)user {
    self.usernameLabel.text = user.userName;
    if (user.currentAge) {
        self.ageLabel.text = [NSString stringWithFormat:@"%@", user.currentAge];
    } else {
        self.ageLabel.text = @"";
    }
    
    self.cityLabel.text = user.city;
    self.stateCountryLabel.text = [NSString stringWithFormat:@"%@, %@", user.state, user.country];
    
    self.noteButton.hidden = user.notes.length == 0;
    
    [self.profilePhotoImageView sd_setImageWithURL:[NSURL URLWithString:user.profilePicUrl] placeholderImage:[UIImage imageNamed:@""]];
}

- (IBAction)noteButtonPressed:(UIButton *)sender {
    DDLogDebug(@"%s", __PRETTY_FUNCTION__);
    [self.delegate notesPressedForCell:self];
}


@end
