//
//  UserDetailsViewController.m
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "UserDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Toasts.h"
#import "User+Helpers.h"

@interface UserDetailsViewController () <UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateCountryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UITextView *notesTextView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyfatLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewBottomLayoutConstraint;
@property (nonatomic) BOOL textViewShouldShowPlaceholderText;
@end

@implementation UserDetailsViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    for (UIButton *button in @[self.saveButton, self.clearButton]) {
        button.clipsToBounds = YES;
        button.layer.cornerRadius = 8.0f;
        button.layer.borderWidth = 1.0f;
        button.layer.borderColor = [UIColor whiteColor].CGColor;
    }
    self.saveButton.backgroundColor = [UIColor moneyGreenColor];
    self.clearButton.backgroundColor = [UIColor pastelBlueColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.view addSubview:self.scrollView];
    [self.view layoutIfNeeded];
    
    self.navigationItem.title = NSLocalizedString(@"Member", @"Member");
    [self setupLabelValues];
    [self setupTextView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.notesTextView resignFirstResponder];
    self.scrollView.frame = self.parentViewController.view.bounds;
}

#pragma mark - Setup
- (void)setupLabelValues {
    self.usernameLabel.text = self.user.userName;
    self.cityLabel.text = self.user.city;
    self.stateCountryLabel.text = [NSString stringWithFormat:@"%@, %@", self.user.state, self.user.country];
    self.nameLabel.text = self.user.realName;
    self.ageLabel.text = [NSString stringWithFormat:@"%@", self.user.currentAge];
    NSDictionary *height = self.user.heightDictionaryImperial;
    self.heightLabel.text = [NSString stringWithFormat:@"%@' %@\"", height[kUserHeightFootKey], height[kUserHeightInchesKey]];
    self.weightLabel.text = [NSString stringWithFormat:@"%@ lbs", self.user.weight];
    self.bodyfatLabel.text = [NSString stringWithFormat:@"%@%%", self.user.bodyfat];
    
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.user.profilePicUrl]];
}

- (void)setupTextView {
    self.notesTextView.delegate = self;
    if (self.user.notes && self.user.notes.length > 0) {
        self.textViewShouldShowPlaceholderText = NO;
        self.notesTextView.text = self.user.notes;
    } else {
        self.textViewShouldShowPlaceholderText = YES;
        self.notesTextView.text = NSLocalizedString(@"Notes...", @"Notes...");
        self.notesTextView.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark - Implementation
- (void)keyboardDidShow:(NSNotification *)notification {
    NSDictionary *dictionary = notification.userInfo;
    
    CGSize keyboardSize = [[dictionary objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewBottomLayoutConstraint.constant = (keyboardSize.height + 24);
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.scrollView scrollRectToVisible:self.saveButton.frame animated:YES];
    }];
}

- (void)keyboardDidHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        self.viewBottomLayoutConstraint.constant = 0;
        [self.view layoutIfNeeded];
    }];
}
#pragma mark - Actions
- (IBAction)saveButtonPressed:(UIButton *)sender {
    [self saveTextToNotes:self.notesTextView.text];
}

- (IBAction)clearButtonPressed:(UIButton *)sender {
    self.notesTextView.text = @"";
    [self saveTextToNotes:self.notesTextView.text];
}

- (void)saveTextToNotes:(NSString *)text {
    self.user.notes = text;

    [[ANDYDataManager sharedManager] persistContext];
    [Toasts showSuccessNotificationWithMessage:NSLocalizedString(@"Notes Saved!", @"Notes Saved!")];
    [self.notesTextView resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (self.textViewShouldShowPlaceholderText) {
        self.textViewShouldShowPlaceholderText = NO;
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}
- (void)textViewDidChange:(UITextView *)textView {
    self.user.notes = textView.text;
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    self.user.notes = textView.text;
    if ([textView.text isEqualToString:@""] ||  textView.text.length < 1) {
        self.textViewShouldShowPlaceholderText = YES;
        textView.text = NSLocalizedString(@"Comments...", @"Comments...");
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

@end
