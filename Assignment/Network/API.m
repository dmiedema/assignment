//
//  API.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "API.h"
#import "NetworkCalls.h"

NSString * NSStringForAPIMemberSort(APIMemeberSort sort) {
    NSString *str;
    switch (sort) {
        case APIMemeberSortUserNameASC:
            str = @"UserName ASC";
            break;
        case APIMemeberSortUserNameDESC:
            str = @"UserName DESC";
            break;
        case APIMemeberSortAgeASC:
            str = @"Age ASC";
            break;
        case APIMemeberSortAgeDESC:
            str = @"Age DESC";
            break;
        default:
            break;
    }
    return str;
}

NSString * URLParamStringForAPIMemberSort(APIMemeberSort sort) {
    NSString *str;
    switch (sort) {
        case APIMemeberSortUserNameASC:
            str = @"userName ASC";
            break;
        case APIMemeberSortUserNameDESC:
            str = @"userName DESC";
            break;
        case APIMemeberSortAgeASC:
            str = @"birthday ASC";
            break;
        case APIMemeberSortAgeDESC:
            str = @"birthday DESC";
            break;
        default:
            break;
    }
    return str;
}

@implementation API

+ (void)membersWithLimit:(NSInteger)limit offset:(NSInteger)offset sort:(APIMemeberSort)sort success:(UsersFetchSuccessBlock)success failure:(UsersFetchFailureBlock)failure {
    
    NSString *_sort = URLParamStringForAPIMemberSort(sort);
    NSDictionary *params = @{@"limit": @(limit),@"skip": @(offset), @"sort": _sort};
    
    [[NetworkCalls manager] GET:@"member" parameters:params success:^(NSURLSessionDataTask *task, id responseObject) {
        success(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        failure(error);
    }];
}

@end
