//
//  UsersTableViewController.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

// TableView Stuff
#import "UsersTableViewController.h"
#import "UsersTableViewDataSource.h"
// Model
#import "User.h"
// Cells
#import "UsersTableHeaderView.h"
#import "UsersTableViewCell.h"
#import "UsersTableFooterView.h"
// Navigation To Controllers
#import "UserDetailsViewController.h"
#import "NotesViewController.h"

@interface UsersTableViewController () <UsersTableViewDataSourceProtocol, UsersTableHeaderViewProtocol, UsersTableViewCellProtocol>
@property (strong, nonatomic) UsersTableViewDataSource *dataSource;
@property (strong, nonatomic) UsersTableHeaderView *headerView;
@property (strong, nonatomic) UsersTableFooterView *footerView;
@property (strong, nonatomic) NotesViewController *notesViewController;
@property (nonatomic) BOOL hitLastPage;
@end

@implementation UsersTableViewController
#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTableView];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.hitLastPage = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = NSLocalizedString(@"Memebers", @"Members");
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.dataSource loadNextPage];
}

#pragma mark - Setup
- (void)setupTableView {
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass(UsersTableViewCell.class) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:CellReuseIdentifier];
    
    void (^configureCell)(UsersTableViewCell *, User *) = ^(UsersTableViewCell *cell, User *user){
        cell.delegate = self;
        [cell setupCell:user];
    };
    
    self.dataSource = [UsersTableViewDataSource dataSourceWithConfigurationBlock:configureCell];
    
    self.tableView.dataSource = self.dataSource;
    self.dataSource.delegate = self;
}
#pragma mark - Getters
- (UsersTableHeaderView *)headerView {
    if (!_headerView) {
        _headerView = [UsersTableHeaderView headerView];
        _headerView.delegate = self;
    }
    return _headerView;
}
- (UsersTableFooterView *)footerView {
    if (!_footerView) {
        _footerView = [UsersTableFooterView footerView];
    }
    return _footerView;
}
- (NotesViewController *)notesViewController {
    if (!_notesViewController) {
        _notesViewController = [[NotesViewController alloc] init];
    }
    return _notesViewController;
}
#pragma mark - TableView Header/Footer Views
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return  nil ; // self.footerView;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 98;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return 46;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section {
    return 0; // 30;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogDebug(@"Selected Cell -%@", indexPath);
    User *user = [self.dataSource itemAtIndexPath:indexPath];
    UserDetailsViewController *details = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass(UserDetailsViewController.class)];
    details.user = user;
    [self.navigationController pushViewController:details animated:YES];
}

#pragma mark - UsersTableViewDataSourceProtocol
- (void)membersLoadedSuccessfully {
    [self.tableView reloadData];
}

- (void)membersFailedToLoad:(NSError *)error {
    
}

- (void)loadingMoreUsers {
    [self.footerView showMore];
}

- (void)lastPageReached {
    self.hitLastPage = YES;
    [self.footerView showNoMore];
}

#pragma mark - UsersTableHeaderViewProtocol
- (void)HeaderViewSelectedIndex:(UsersTableViewSegmentSelected)selectedSegment {
    DDLogDebug(@"Selected Index %@", NSStringFromUsersTableViewSelectedSegment(selectedSegment));
    self.dataSource.sort = (APIMemeberSort)selectedSegment;
    [self.tableView reloadData];
}

#pragma mark - UsersTableViewCellProtocol
- (void)notesPressedForCell:(UsersTableViewCell *)cell {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    self.notesViewController.user = [self.dataSource itemAtIndexPath:indexPath];
    
    UIViewController *rootController = [UIApplication sharedApplication].keyWindow.rootViewController;
    rootController.view.backgroundColor = [UIColor clearColor];
    [rootController addChildViewController:self.notesViewController];
    [rootController.view addSubview:self.notesViewController.view];
    
    [self.notesViewController showNotesView:YES];
}

@end
