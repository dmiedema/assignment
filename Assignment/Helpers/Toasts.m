//
//  Toasts.m
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "Toasts.h"
#import <CRToast/CRToast.h>

NSString * const kDMMToastSuccessToastIdentifer  = @"kDMMToastSuccessToastIdentifer";
NSString * const kDMMToastErrorToastIdentifer    = @"kDMMToastErrorToastIdentifer";

CRToastInteractionResponder * DMMToastSwipeInteractionResponder(void) {
    return [CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeSwipe automaticallyDismiss:YES block:nil];
}

NSMutableDictionary * DMMToastSuccessOptions(void) {
    return [@{kCRToastTextKey : NSLocalizedStringFromTable(@"Success", NSStringFromClass(Toasts.class), @"Success"),
              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
              kCRToastSubtitleTextAlignmentKey: @(NSTextAlignmentCenter),
              kCRToastFontKey: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline],
              kCRToastSubtitleFontKey: [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline],
              kCRToastStatusBarStyleKey: @(UIStatusBarStyleLightContent),
              kCRToastBackgroundColorKey : [UIColor pastelGreenColor],
              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionLeft),
              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionRight),
              kCRToastTimeIntervalKey: @1,
              kCRToastNotificationTypeKey: @(CRToastTypeStatusBar),
              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
              } mutableCopy];
}

NSMutableDictionary * DMMToastErrorOptions(void) {
    return [@{kCRToastTextKey : NSLocalizedStringFromTable(@"Something went wrong", NSStringFromClass(Toasts.class), @"Something went wrong"),
              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
              kCRToastSubtitleTextAlignmentKey: @(NSTextAlignmentCenter),
              kCRToastFontKey: [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline],
              kCRToastSubtitleFontKey: [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline],
              kCRToastStatusBarStyleKey: @(UIStatusBarStyleLightContent),
              kCRToastSubtitleTextKey : NSLocalizedString(@"Swipe to dismiss", @"swipe to dismiss"),
              kCRToastBackgroundColorKey : [UIColor brickRedColor],
              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
              kCRToastTimeIntervalKey: @5,
              kCRToastNotificationTypeKey: @(CRToastTypeNavigationBar),
              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeSpring),
              } mutableCopy];
}

@implementation Toasts

+ (void)showSuccessNotificationWithMessage:(NSString *)message {
    [Toasts showSuccessNotificationWithMessage:message completionBlock:nil];
}


+ (void)showErrorNotificationWithMessage:(NSString *)message {
    [Toasts showErrorNotificationWithMessage:message completionBlock:nil];
}

+ (void)showSuccessNotificationWithMessage:(NSString *)message completionBlock:(void (^)(void))completion {
    CRToastInteractionResponder *swipeAction = DMMToastSwipeInteractionResponder();
    NSMutableDictionary *options = DMMToastSuccessOptions();
    
    options[kCRToastInteractionRespondersKey] = @[swipeAction];
    
    NSString *identifier;
    if (message) {
        identifier = message;
        options[kCRToastTextKey] = message;
        options[kCRToastIdentifierKey] = options[kCRToastIdentifierKey] ?: identifier;
    } else {
        identifier = kDMMToastSuccessToastIdentifer;
        options[kCRToastIdentifierKey] = options[kCRToastIdentifierKey] ?: identifier;
    }
    
    if ([self managerIsShowingNotificationForIdentifier:identifier]) { return; }
    
    [Toasts showNotificationWithOptions:options completionBlock:completion];
}

+ (void)showErrorNotificationWithMessage:(NSString *)message completionBlock:(void (^)(void))completion {
    CRToastInteractionResponder *swipeAction = DMMToastSwipeInteractionResponder();
    NSMutableDictionary *options = DMMToastErrorOptions();
    
    options[kCRToastInteractionRespondersKey] = @[swipeAction];
    
    NSString *identifier;
    if (message) {
        identifier = message;
        options[kCRToastTextKey] = message;
        options[kCRToastIdentifierKey] = options[kCRToastIdentifierKey] ?: identifier;
    } else {
        identifier = kDMMToastErrorToastIdentifer;
        options[kCRToastIdentifierKey] = options[kCRToastIdentifierKey] ?: identifier;
    }
    
    if ([self managerIsShowingNotificationForIdentifier:identifier]) { return; }
    
    [Toasts showNotificationWithOptions:options completionBlock:nil];
}

+ (void)showNotificationWithOptions:(NSDictionary *)options completionBlock:(void (^)(void))completion {
    [CRToastManager showNotificationWithOptions:options completionBlock:completion];
}

#pragma mark - Private
+ (BOOL)managerIsShowingNotificationForIdentifier:(NSString *)identifier {
    __block BOOL alreadyShowing = NO;
    [[CRToastManager notificationIdentifiersInQueue] enumerateObjectsUsingBlock:^(NSString *ident, NSUInteger idx, BOOL *stop) {
        if ([identifier isEqualToString:ident]) {
            alreadyShowing = YES;
            *stop = YES;
        }
    }];
    
    return alreadyShowing;
}

@end
