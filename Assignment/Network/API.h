//
//  API.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Enum to define how users should be sorted.
 */
typedef NS_ENUM(NSInteger, APIMemeberSort){
    APIMemeberSortUserNameASC = 0,
    APIMemeberSortUserNameDESC,
    APIMemeberSortAgeASC,
    APIMemeberSortAgeDESC
};
/*!
 NSString Representation of APIMemberSort for debugging
 */
NSString * NSStringForAPIMemberSort(APIMemeberSort sort);

/*!
 NSString Representation of APIMemberSort compatible with being used in URL params
 */
NSString * URLParamStringForAPIMemberSort(APIMemeberSort sort);

typedef void(^UsersFetchSuccessBlock)(NSArray *responseObject);
typedef void(^UsersFetchFailureBlock)(NSError *error);

@interface API : NSObject

/*!
 Load members with a given offset, limit of users and specified sort.
 
 @note maximum limit is @c 10. If any number larger than @c 10 is passed, @c 10 will be used
 
 @param  limit  number of members to fetch at once. Used as @c limit param in request
 @param  offset offset to begin fetching users at. Used as @c offset param in request
 @param  sort   sort order to request members in. Used as the @c sort param in request
 @param  success block to execute upon successful network request
 @param  failure block to execute upon failed network request
 */
+ (void)membersWithLimit:(NSInteger)limit offset:(NSInteger)offset sort:(APIMemeberSort)sort success:(UsersFetchSuccessBlock)success failure:(UsersFetchFailureBlock)failure;

@end
