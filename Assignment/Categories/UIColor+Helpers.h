//
//  UIColor+Helpers.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Helpers)

/*!
 Wrapper around @c colorWithRed:green:blue:alpha to automatically divide values
 by @c 255.0
 */
+ (UIColor *)dmm_colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

@end
