//
//  UserDetailsViewController.h
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;

@interface UserDetailsViewController : UIViewController
@property (strong, nonatomic) User *user;
@end
