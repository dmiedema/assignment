# Bodybuilding.com Assignment

## Daniel Miedema -- iOS

### Information

- I used [CocoaPods](http://cocoapods.org) to manage my dependencies just to keep things easy. I prefer Cocoapods over submodules personally.

#### Explanation of Pods Used

- `AFNetworking`: Simplifiy networking calls & use.
- `ANDYDataManager`: Save me a little time with CoreData stack. Could very easily be removed I just decided to use it to keep things simpler especially for testing
- `SDWebImage`: Using it for `UIImageView` category to cache images & download asynchronously. I could have written my own category with `GCD` and `NSCache` to do the same thing but in an effort to not reinvent the wheel I opted for this one.
- `CRToast`: personal fork of [CRToast](https://github.com/cruffenach/CRToast). I really like it for subtle user feed back. Used when notes are saved on a member
- `Colours`: Add extra colors to `[UIColor ____]` color definitions. Mostly just to give some quick variety. Not important & could be pulled easily
- `CocoaLumberjack`: `NSLog++` kind of. I use it heavily for debugging since it supports colors & various log levels.

- `OCMock`: Library I always include in the test suite for mocking out delegates/controllers so its easier to test a single object at a time.

### Setup

In terminal

`pod install && open Assignment.xcworkspace`

