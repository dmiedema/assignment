//
//  NSObject+HelpersTests.m
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+Helpers.h"
#import <XCTest/XCTest.h>

@interface NSObject_HelpersTests : XCTestCase

@end

@implementation NSObject_HelpersTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testNSNullIsNil {
    XCTAssertNil(DMMObjectOrNull([NSNull null]), @"[NSNull null] should evaluate to nil");
}

- (void)testEmptyStringIsNotNil {
    XCTAssertNotNil(DMMObjectOrNull(@""), @"Empty string should not be nil");
}

@end
