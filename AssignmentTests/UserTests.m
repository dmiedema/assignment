//
//  UserTests.m
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "User+Helpers.h"
#import <ANDYDataManager/ANDYDataManager.h>

@interface User (Tests)
+ (instancetype)userForuserID:(NSNumber *)userId inContext:(NSManagedObjectContext *)context;
@end

@interface UserTests : XCTestCase
@end

NSDictionary * TestUserDictionary(void) {
    return @{
             @"birthday": @"1989-03-24",
             @"bodyfat": @8,
             @"city": @"Sydney",
             @"country": @"AU",
             @"createdAt": @"2014-10-23T22:27:27.414Z",
             @"height": @73,
             @"id": @3,
             @"profilePicUrl": @"http://common.bbcomcdn.com/images/default-avatar/Avatar-Male-Default_100x100.jpg",
             @"realName": @"Aziz Shavershian",
             @"state": @"NSW",
             @"updatedAt": @"2014-10-23T22:27:27.414Z",
             @"userId": @123840,
             @"userName": @"Zyzz",
             @"weight": @205
             };
}

NSDictionary * TestRandomUserDictionary(void) {
    return @{
             @"birthday": @"1989-03-24",
             @"bodyfat": @8,
             @"city": @"Sydney",
             @"country": @"AU",
             @"createdAt": @"2014-10-23T22:27:27.414Z",
             @"height": @73,
             @"id": @(arc4random() % 256256),
             @"profilePicUrl": @"http://common.bbcomcdn.com/images/default-avatar/Avatar-Male-Default_100x100.jpg",
             @"realName": @"Aziz Shavershian",
             @"state": @"NSW",
             @"updatedAt": @"2014-10-23T22:27:27.414Z",
             @"userId": @(arc4random() % 256256),
             @"userName": @"Zyzz",
             @"weight": @205
             };
}

@implementation UserTests

- (void)setUp {
    [super setUp];
    [ANDYDataManager setUpStackWithInMemoryStore];
}

- (void)tearDown {
    [[ANDYDataManager sharedManager] destroy];
    [super tearDown];
}

- (void)testUserLoadsByID {
    NSDictionary *data = TestUserDictionary();
    [User createOrUpdateWithData:data inContext:[ANDYDataManager sharedManager].mainContext];
    
    XCTAssertNotNil([User userForuserID:data[@"userId"] inContext:[ANDYDataManager sharedManager].mainContext], @"A User should be found for the ID already");
}

- (void)testUserCreatesWhenIDIsNotPresent {
    NSDictionary *data = TestRandomUserDictionary();
    [User createOrUpdateWithData:data inContext:[ANDYDataManager sharedManager].mainContext];
    
    XCTAssertNil([User userForuserID:TestRandomUserDictionary()[@"userId"] inContext:[ANDYDataManager sharedManager].mainContext], @"A User should not be found for a different ID");
}

- (void)testHeightDictionaryCalculation {
    NSDictionary *data = TestUserDictionary();
    User *user =[User createOrUpdateWithData:data inContext:[ANDYDataManager sharedManager].mainContext];
    
    NSDictionary *height = [user heightDictionaryImperial];
    
    XCTAssertEqual([height[kUserHeightFootKey] integerValue], 6, @"User of 73 inches should be 6 feet tall");
    XCTAssertEqual([height[kUserHeightInchesKey] integerValue], 1, @"User of 73 inches should be 1 inch");
}

- (void)testAgeCalculation {
    NSDictionary *data = TestUserDictionary();
    User *user =[User createOrUpdateWithData:data inContext:[ANDYDataManager sharedManager].mainContext];
    
    NSInteger age = [[user currentAge] integerValue];
    
    XCTAssertEqual(age, 25, @"User should be 25 years old via calcuation");
}

@end
