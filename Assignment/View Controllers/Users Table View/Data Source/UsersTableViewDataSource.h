//
//  UsersTableViewDataSource.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "API.h"
@protocol UsersTableViewDataSourceProtocol;

/// UsersTableView Header Reuse identifer
extern NSString * const HeaderReuseIdentifier;
/// UsersTableView Cell Reuse identifer
extern NSString * const CellReuseIdentifier;
/// UsersTableView Footer Reuse identifer
extern NSString * const FooterReuseIdentifier;

/// Configuration block to run when creating cell
typedef void (^DMMUsersTableViewCellConfigurationBlock)(id cell, id item);

@interface UsersTableViewDataSource : NSObject <UITableViewDataSource>
/// current items in the data source
@property (nonatomic, readonly) NSArray *items;
/// Set number of items to fetch per page. Max 10. Default is 5
@property (nonatomic) NSInteger perPageFetch;
/// Set the sort order for the fetches. Default is userName ASC
@property (nonatomic) APIMemeberSort sort;
/// delegate to respond to @c UsersTableViewDataSourceProtocol methods
@property (weak, nonatomic) id<UsersTableViewDataSourceProtocol> delegate;

#pragma mark - Class Methods
/*!
 Setup a TableView Data Source with specified configuration block
 @param configurationBlock configuration block to use when @c tableView:cellForRowAtIndexPath is called
 */
+ (instancetype)dataSourceWithConfigurationBlock:(DMMUsersTableViewCellConfigurationBlock)configurationBlock;

#pragma mark - Instance Methods
/*!
 Get the item at a specified index path
 
 @param  indexPath item to get at specified indexPath
 @return the item at that index path, whatever it is
 */
- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

/*!
 Tell the datasource to load the next page
 */
- (void)loadNextPage;

/*!
 Reset current data source contents
 */
- (void)resetCurrentData;

/*!
 Calls @c resetCurrentData and calls @c loadNextPage
 */
- (void)reloadData;
@end

#pragma mark - Protocol
@protocol UsersTableViewDataSourceProtocol <NSObject>

@required
/*!
 Tell the delegate members loaded successfully
 */
- (void)membersLoadedSuccessfully;
/*!
 Tell the delegate loading failed with an error
 */
- (void)membersFailedToLoad:(NSError *)error;

@optional
/*!
 Tell the delegate we are currently loading
 */
- (void)loadingMoreUsers;
/*!
 Tell the delegate the last page has been reached
 */
- (void)lastPageReached;
@end
