//
//  NotesViewController.h
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  User;
@interface NotesViewController : UIViewController

@property (strong, nonatomic) User *user;

/*!
 Show the notes view
 
 @param animated @c YES if it should animate in, @c NO otherwise
 */
- (void)showNotesView:(BOOL)animated;

/*!
 Dismiss the notes view
 
 @param animated @c YES if it should animate. @c NO otherwise.
 */
- (void)dismissNotesView:(BOOL)animated;
@end
