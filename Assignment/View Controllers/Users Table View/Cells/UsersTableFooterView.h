//
//  UsersTableFooterView.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsersTableFooterView : UITableViewHeaderFooterView

/*!
 Create footer view
 */
+ (instancetype)footerView;

/*!
 Show activity indicator in footer.
 */
- (void)showSpinner;

/*!
 Show 'Scroll Down For More' in label in footer
 */
- (void)showMore;

/*!
 Show 'No More Results' in label in footer
 */
- (void)showNoMore;
@end
