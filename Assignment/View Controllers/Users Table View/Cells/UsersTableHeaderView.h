//
//  UsersTableHeaderView.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 */
@protocol UsersTableHeaderViewProtocol;

/*!
 */
typedef NS_ENUM(NSInteger, UsersTableViewSegmentSelected){
    UsersTableViewSegmentSelectedNameASC = 0,
    UsersTableViewSegmentSelectedNameDESC,
    UsersTableViewSegmentSelectedAgeASC,
    UsersTableViewSegmentSelectedAgeDESC
};
/*!
 NSString representation of UsersTableViewSegmentSelected) for Debugging
 */
NSString * NSStringFromUsersTableViewSelectedSegment(UsersTableViewSegmentSelected);

@interface UsersTableHeaderView : UITableViewHeaderFooterView

/*!
 Delegate to listen to @c UsersTableHeaderViewProtocol method
 */
@property (weak, nonatomic) id<UsersTableHeaderViewProtocol> delegate;

/*!
 Create new header view.
 */
+ (instancetype)headerView;
@end

@protocol UsersTableHeaderViewProtocol <NSObject>
@required
/*!
 */
- (void)HeaderViewSelectedIndex:(UsersTableViewSegmentSelected)selectedSegment;

@end
