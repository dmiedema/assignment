//
//  AppDelegate.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "AppDelegate.h"
#import <CocoaLumberjack/CocoaLumberjack.h>



@interface AppDelegate ()
- (void)configureLogging;
@end

@implementation AppDelegate

#pragma mark - Application Life Cycle
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self configureLogging];
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [[ANDYDataManager sharedManager] persistContext];
}

#pragma mark - Configuration
- (void)configureLogging {
    [DDLog addLogger:[DDASLLogger sharedInstance] withLevel:ddLogLevel];
    [DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:ddLogLevel];
    
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blueColor] backgroundColor:nil forFlag:DDLogFlagInfo];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor purpleColor] backgroundColor:nil forFlag:DDLogFlagDebug];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor dmm_colorWithRed:220 green:10 blue:45 alpha:1.0] backgroundColor:nil forFlag:DDLogFlagVerbose];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor redColor] backgroundColor:[UIColor blackColor] forFlag:DDLogFlagError];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor dmm_colorWithRed:216 green:188 blue:68 alpha:1] backgroundColor:[UIColor blackColor] forFlag:DDLogFlagWarning];

}

@end
