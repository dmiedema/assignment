//
//  UsersTableViewCell.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <UIKit/UIKit.h>
@class User;

@protocol UsersTableViewCellProtocol;

@interface UsersTableViewCell : UITableViewCell
@property (weak, nonatomic) id<UsersTableViewCellProtocol> delegate;
/*!
 Tell cell to setup its contents as necessary with specified user object
 
 @param user @c User object to run setup with
 */
- (void)setupCell:(User *)user;
@end

@protocol UsersTableViewCellProtocol <NSObject>
/*!
 Inform the delegate notes was pressed
 */
- (void)notesPressedForCell:(UsersTableViewCell *)cell;
@end
