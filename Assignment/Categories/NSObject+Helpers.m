//
//  NSObject+Helpers.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "NSObject+Helpers.h"

id DMMObjectOrNull(id object) {
    if ([object isEqual:[NSNull null]] || !object) {
        return nil;
    }
    return object;
}

@implementation NSObject (Helpers)
@end
