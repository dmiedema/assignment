//
//  User+Helpers.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "User+Helpers.h"

NSString * const kUserHeightFootKey   = @"kUserHeightFootKey";
NSString * const kUserHeightInchesKey = @"kUserHeightInchesKey";

@implementation User (Helpers)

#pragma mark - Public
+ (instancetype)createOrUpdateWithData:(NSDictionary *)data inContext:(NSManagedObjectContext *)context {
    User *user = [User userForuserID:data[@"userId"] inContext:context];
    
    if (!user) {
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    
    /* Format is as follows
     Dictionary Value from JSON -> Previous Value -> Default Value */
    
    
    user.createdAt = [dateFormatter dateFromString:DMMObjectOrNull(data[@"createdAt"]) ?: user.createdAt ?: [NSDate date]];
    user.updatedAt = [dateFormatter dateFromString:DMMObjectOrNull(data[@"updatedAt"]) ?: user.updatedAt ?: [NSDate date]];
    
    dateFormatter.dateFormat = @"yyy-MM-dd";
    if (DMMObjectOrNull(data[@"birthday"])) {
        user.birthday = [dateFormatter dateFromString:DMMObjectOrNull(data[@"birthday"]) ?: user.birthday ?: [NSDate date]];
    }

    user.bodyfat       = DMMObjectOrNull(data[@"bodyfat"])       ?: user.bodyfat       ?: 0;
    user.profilePicUrl = DMMObjectOrNull(data[@"profilePicUrl"]) ?: user.profilePicUrl ?: @"";
    user.city          = DMMObjectOrNull(data[@"city"])          ?: user.city          ?: @"";
    user.state         = DMMObjectOrNull(data[@"state"])         ?: user.state         ?: @"";
    user.country       = DMMObjectOrNull(data[@"country"])       ?: user.country       ?: @"";
    user.id            = DMMObjectOrNull(data[@"id"])            ?: user.id;           // no default
    user.height        = DMMObjectOrNull(data[@"height"])        ?: user.height        ?: @0;
    user.realName      = DMMObjectOrNull(data[@"realName"])      ?: user.realName      ?: @"";
    user.userId        = DMMObjectOrNull(data[@"userId"])        ?: user.userId;       // no default
    user.weight        = DMMObjectOrNull(data[@"weight"])        ?: user.weight        ?: @0;
    user.userName      = DMMObjectOrNull(data[@"userName"])      ?: user.userName      ?: @"";
    
    return user;
}

- (NSNumber *)currentAge {
    if (!self.birthday) { return nil; }
    NSCalendar *gregorianCalendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:(NSCalendarUnitYear) fromDate:self.birthday toDate:[NSDate date] options:0];
    return @(components.year);
    
    /* // Playground
     var str = "1982-05-14"
     
     var dateFormatter = NSDateFormatter()
     dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
     dateFormatter.dateFormat = "yyyy-MM-dd"
     
     let bday = dateFormatter.dateFromString("1982-05-14")
     
     let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
     
     let day = NSDate()
     
     let then = calendar?.components(.CalendarUnitYear, fromDate: bday!).year
     let now = calendar?.components(.CalendarUnitYear, fromDate: NSDate()).year
     now! - then!
    */
}

- (NSDictionary *)heightDictionaryImperial {
    NSInteger height = self.height.integerValue;
    NSInteger feet   = height / 12;
    NSInteger inches = height % 12;
    return @{kUserHeightFootKey: @(feet), kUserHeightInchesKey: @(inches)};
}

#pragma mark - Private
+ (instancetype)userForuserID:(NSNumber *)userId inContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId = %@", userId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *user = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        DDLogError(@"Error loading user by ID. %@, %@", error, error.userInfo);
    }
    
    if (!user) {
        return nil;
    }
    
    return [user lastObject];
}
@end
