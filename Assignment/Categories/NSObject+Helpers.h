//
//  NSObject+Helpers.h
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Helper to check to see if an object is @c nil or @c [NSNull @c null]
 *
 *  @param object to check
 *  @return @c id the object if not nil or of type @c [NSNull @c null].  @c nil otherwise.
 */
id DMMObjectOrNull(id object);

@interface NSObject (Helpers)
@end
