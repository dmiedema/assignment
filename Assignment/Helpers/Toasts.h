//
//  Toasts.h
//  Assignment
//
//  Created by Daniel on 1/14/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import <Foundation/Foundation.h>

/// Default Identifier used for a success toast
extern NSString * const kDMMToastSuccessToastIdentifer;

/// Default Identifier used for a error toast
extern NSString * const kDMMToastErrorToastIdentifer;

@interface Toasts : NSObject
/*!
 Create & show a generic success notification with swipe to dismiss & specific message
 
 @param message to put into top line of text
 */
+ (void)showSuccessNotificationWithMessage:(NSString *)message;

/*!
 Create & show a generic error notification with swipe to dismiss responder & specifc message
 
 @param message to put into top line of text
 */
+ (void)showErrorNotificationWithMessage:(NSString *)message;
@end
