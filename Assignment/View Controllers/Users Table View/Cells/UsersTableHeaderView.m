//
//  UsersTableHeaderView.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "UsersTableHeaderView.h"

@interface UsersTableHeaderView()
@property (strong, nonatomic) UISegmentedControl *segmentControl;
@end

NSArray * UsersHeaderSegementItems(void) {
    return @[
             NSLocalizedString(@"Username ⇡", @"username ⇡"),
             NSLocalizedString(@"Username ⇣", @"username ⇣"),
             NSLocalizedString(@"Age ⇡", @"age ⇡"),
             NSLocalizedString(@"Age ⇣", @"age ⇣"),
             ];
}

NSString * NSStringFromUsersTableViewSelectedSegment(UsersTableViewSegmentSelected segment) {
    NSString *str;
    switch (segment) {
        case UsersTableViewSegmentSelectedNameASC:
            str = @"Username ⇡";
            break;
        case UsersTableViewSegmentSelectedNameDESC:
            str = @"Username ⇣";
            break;
        case UsersTableViewSegmentSelectedAgeASC:
            str = @"Age ⇡";
            break;
        case UsersTableViewSegmentSelectedAgeDESC:
            str = @"Age ⇣";
            break;
        default:
            break;
    }
    return str;
}

@implementation UsersTableHeaderView
#pragma mark - Class Method
+ (instancetype)headerView {
    return [[self alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
}

#pragma mark - Init
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.segmentControl];
        [self addConstraints:[self segmentControlConstraints]];
    }
    return self;
}

#pragma mark - Getter
- (UISegmentedControl *)segmentControl {
    if (!_segmentControl) {
        _segmentControl = [[UISegmentedControl alloc] initWithItems:UsersHeaderSegementItems()];
        _segmentControl.translatesAutoresizingMaskIntoConstraints = NO;
        [_segmentControl addTarget:self action:@selector(segmentControlChanged:) forControlEvents:UIControlEventValueChanged];
        _segmentControl.selectedSegmentIndex = 0;
    }
    return _segmentControl;
}

- (NSArray *)segmentControlConstraints {
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.segmentControl attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.segmentControl.superview attribute:NSLayoutAttributeLeadingMargin multiplier:1 constant:0];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.segmentControl attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.segmentControl.superview attribute:NSLayoutAttributeTrailingMargin multiplier:1 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.segmentControl attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.segmentControl.superview attribute:NSLayoutAttributeBottomMargin multiplier:1 constant:0];
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.segmentControl attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:28];
    
    return @[leading, trailing, bottom, height];
}

#pragma mark - Actions
- (void)segmentControlChanged:(UISegmentedControl *)sender {
    self.segmentControl.selectedSegmentIndex = sender.selectedSegmentIndex;
    [self.delegate HeaderViewSelectedIndex:sender.selectedSegmentIndex];
}
@end
