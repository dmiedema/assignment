//
//  UsersTableViewDataSource.m
//  Assignment
//
//  Created by Daniel on 1/13/15.
//  Copyright (c) 2015 dmiedema. All rights reserved.
//

#import "UsersTableViewDataSource.h"
#import "User+Helpers.h"

NSString * const HeaderReuseIdentifier = @"HeaderReuseIdentifier";
NSString * const CellReuseIdentifier   = @"CellReuseIdentifier";
NSString * const FooterReuseIdentifier = @"FooterReuseIdentifier";

@interface UsersTableViewDataSource()
@property (strong, nonatomic) NSMutableArray *mutableItems;
@property (copy, nonatomic) DMMUsersTableViewCellConfigurationBlock configurationBlock;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) BOOL lastPage;

- (instancetype)initWithConfigurationBlock:(DMMUsersTableViewCellConfigurationBlock)aBlock;
- (BOOL)isLastRow:(NSIndexPath *)indexPath;
@end

NSString * KeyForAPIMemberSortKey(APIMemeberSort sort) {
    NSString *str;
    switch (sort) {
        case APIMemeberSortUserNameASC:
            str = @"userName";
            break;
        case APIMemeberSortUserNameDESC:
            str = @"userName";
            break;
        case APIMemeberSortAgeASC:
            str = @"birthday";
            break;
        case APIMemeberSortAgeDESC:
            str = @"birthday";
            break;
        default:
            break;
    }
    return str;
}

BOOL APIMemberSortAscending(APIMemeberSort sort) {
    BOOL asc = YES;
    switch (sort) {
        case APIMemeberSortUserNameASC:
            asc = YES;
            break;
        case APIMemeberSortUserNameDESC:
            asc = NO;
            break;
        case APIMemeberSortAgeASC:
            asc = YES;
            break;
        case APIMemeberSortAgeDESC:
            asc = NO;
            break;
        default:
            break;
    }
    return asc;
}

@implementation UsersTableViewDataSource
#pragma mark - Class Methods
+ (instancetype)dataSourceWithConfigurationBlock:(DMMUsersTableViewCellConfigurationBlock)configurationBlock {
    return [[self alloc] initWithConfigurationBlock:configurationBlock];
}
#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell = [tableView dequeueReusableCellWithIdentifier:CellReuseIdentifier forIndexPath:indexPath];
    
    if ([self isLastRow:indexPath]) {
        ((UITableViewCell *)cell).contentView.alpha = 0.0;
        DDLogDebug(@"Hit Last Row");
        [self loadNextPage];
    } else {
        ((UITableViewCell *)cell).contentView.alpha = 1.0;
        id item = [self itemAtIndexPath:indexPath];
        self.configurationBlock(cell, item);
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.mutableItems.count;
    if (!self.lastPage) {
        count++;
    }
    return count;
}

#pragma mark - Instance Methods
#pragma mark Setters
- (void)setPerPageFetch:(NSInteger)perPageFetch {
    if (perPageFetch > 10) {
        perPageFetch = 10;
    } else if (perPageFetch < 1) {
        perPageFetch = 1;
    }
    _perPageFetch = perPageFetch;
}

- (void)setSort:(APIMemeberSort)sort {
    _sort = sort;
    [self reloadData];
}

#pragma mark Getters
- (NSArray *)items {
    NSArray *items = [self.mutableItems copy];
    NSSortDescriptor *descriptor;
    
    switch (self.sort) {
        case APIMemeberSortUserNameASC:
            descriptor = [NSSortDescriptor sortDescriptorWithKey:@"userName" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
            break;
        case APIMemeberSortUserNameDESC:
            descriptor = [NSSortDescriptor sortDescriptorWithKey:@"userName" ascending:NO selector:@selector(localizedCaseInsensitiveCompare:)];
            break;
        case APIMemeberSortAgeASC:
            descriptor = [NSSortDescriptor sortDescriptorWithKey:@"self.currentAge" ascending:YES];
            break;
        case APIMemeberSortAgeDESC:
            descriptor = [NSSortDescriptor sortDescriptorWithKey:@"self.currentAge" ascending:NO];
            break;
        default:
            break;
    }
    
    return [items sortedArrayUsingDescriptors:@[descriptor]];
}

#pragma mark Public
- (id)itemAtIndexPath:(NSIndexPath *)indexPath {
    return self.items[indexPath.row];
}

- (void)loadNextPage {
    DDLogDebug(@"%s", __PRETTY_FUNCTION__);
    if (self.lastPage) {
        [self notifyDelegateLastPage];
        return;
    }
    self.currentPage++;
    
    DDLogVerbose(@"currentPage - %li", self.currentPage);
    
    [self notifyDelegateLoading];
    [API membersWithLimit:self.perPageFetch offset:(self.currentPage * self.perPageFetch) sort:self.sort success:^(NSArray *responseObject) {
        if (responseObject.count == 0) {
            self.lastPage = YES;
            [self notifyDelegateLastPage];
        }
        
        NSManagedObjectContext *context = [ANDYDataManager sharedManager].mainContext;
        for (NSDictionary *data in responseObject) {
            User *user = [User createOrUpdateWithData:data inContext:context];
            
            [self.mutableItems addObject:user];
        }
        [self.delegate membersLoadedSuccessfully];
    } failure:^(NSError *error) {
        [self.delegate membersFailedToLoad:error];
    }];
}

- (void)resetCurrentData {
    [self.mutableItems removeAllObjects];
    self.lastPage = NO;
    self.currentPage = 0;
}

- (void)reloadData {
    [self resetCurrentData];
    [self loadNextPage];
}

#pragma mark Private
- (instancetype)initWithConfigurationBlock:(DMMUsersTableViewCellConfigurationBlock)aBlock {
    self = [super init];
    if (self) {
        self.configurationBlock = aBlock;
        self.mutableItems = [NSMutableArray array];
        self.lastPage = NO;
        self.currentPage = 0;
        self.perPageFetch = 5;
        self.sort = APIMemeberSortUserNameASC;
    }
    return self;
}

- (BOOL)isLastRow:(NSIndexPath *)indexPath {
    return indexPath.row == self.items.count;
}

#pragma mark Delegate Notifying
- (void)notifyDelegateLoading {
    if ([self.delegate respondsToSelector:@selector(loadingMoreUsers)]) {
        [self.delegate loadingMoreUsers];
    }
}
- (void)notifyDelegateLastPage {
    if ([self.delegate respondsToSelector:@selector(lastPageReached)]) {
        [self.delegate lastPageReached];
    }
}
@end
